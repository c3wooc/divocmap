#!/bin/bash
set -euo pipefail

shopt -s globstar

for path in **/*.json; do
    jsontype=$(jq type -e "$path");
    if [ $jsontype != '"object"' ]
    then
        echo "$path is json, but not an object!"
        exit 1
    else
        echo "$path seems to be valid json"
    fi
done
